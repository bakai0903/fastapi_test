from typing import List
from typing import Optional

from fastapi import Request


class PostCreateForm:
    def __init__(self, request: Request):
        self.request: Request = request
        self.errors: List = []
        self.title: Optional[str] = None
        self.content: Optional[str] = None

    async def load_data(self):
        form = await self.request.form()
        self.title = form.get("title")
        self.content = form.get("content")

    def is_valid(self):
        if not self.title:
            self.errors.append("A valid title is required")
        if not self.content:
            self.errors.append("A valid content is required")
        if not self.errors:
            return True
        return False
