from fastapi import APIRouter, HTTPException, Depends, status, Request, responses
from sqlalchemy.orm import Session
from database import get_db
from forms import PostCreateForm
from fastapi.templating import Jinja2Templates
from model import Post
from schema import CreatePost
from posts import create_new_post, update_post, delete_post

templates = Jinja2Templates(directory="templates")

router = APIRouter(
    prefix='/posts',
    tags=['Posts']
)


@router.get('/')
async def get_posts(request: Request, db: Session = Depends(get_db)):
    posts = sorted(db.query(Post).all(), key=lambda x: x.id)
    return templates.TemplateResponse("create_post.html", {"request": request, "posts": posts})


@router.post('/')
async  def create_posts(request: Request, db: Session = Depends(get_db)):
    form = PostCreateForm(request)
    await form.load_data()
    
    if form.is_valid():
        post = CreatePost(**form.__dict__)
        post = create_new_post(post=post, db=db)
        return responses.RedirectResponse(
            f"/posts", status_code=status.HTTP_302_FOUND
        )
    return templates.TemplateResponse("create_post.html", form.__dict__)


@router.post('/update/{id}')
async def update_posts(request: Request, id: int=None, db: Session = Depends(get_db)):
    form = PostCreateForm(request)
    await form.load_data()
    post = update_post(CreatePost(**form.__dict__), db, id)
    return responses.RedirectResponse(
            f"/posts", status_code=status.HTTP_302_FOUND
        )


@router.get('/delete/{id}')
async def delete_posts(request: Request, id: int=None, db: Session = Depends(get_db)):
    post = delete_post(db, id)
    return responses.RedirectResponse(
            f"/posts", status_code=status.HTTP_302_FOUND
        )

