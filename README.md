Welcome to our test project showcasing the use of FastAPI, PostgreSQL, and Docker for creating, updating, and deleting posts.

How to run:
1. Clone the Repository:
git clone https://gitlab.com/bakai0903/fastapi_test.git
2. Go into created directory:
cd fastapi_test
3. Build and Run the Docker Containers:
docker-compose up --build -d
4. Access the FastAPI Application:
http://localhost:8000 in your web browser
