from sqlalchemy.orm import Session
from model import Post
from schema import CreatePost
from fastapi.security.utils import get_authorization_scheme_param


def create_new_post(post: CreatePost, db: Session):
    new_post = Post(**post.dict())
    db.add(new_post)
    db.commit()
    db.refresh(new_post)
    return new_post


def update_post(data: CreatePost, db: Session, id: int):
    post = db.query(Post).filter(Post.id==id).first()
    post.title = data.title
    post.content = data.content
    print(post)
    db.add(post)
    db.commit()
    db.refresh(post)
    return post


def delete_post(db: Session, id: int):
    post = db.query(Post).filter(Post.id==id).delete()
    db.commit()
    return post

